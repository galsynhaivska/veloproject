import React from "react";
import "./item.css";


function Item({item}){
    const {id, title, image, category, description, price, rating} = item;
    return (
        <div className="item-card">
            <div className="div-img">
                <img src={image} alt={title}></img>
            </div>
            <h2 className="item-title">{title}</h2>
            <div className="item-descr">{description}
                <div className="full-descr">{description}</div>
            </div>       
            <div className="item-price">${price.toFixed(2)}</div>
            <div className="btn-shop">Shop</div>
            <div className="item-rating">
                <div className="rating-rate"> Rate: <span className="rate">{rating.rate}</span> </div> 
                <div className="rating-count"> Count: <span className="count">{rating.count}</span></div>
            </div> 
        </div>
    )
}

export default Item
