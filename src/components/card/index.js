import React, {Component} from "react";
import Item from "../item";

import "./card.css";


class Card extends Component{
    state = {
        items: [],
    }

    componentDidMount(){
        const req = new XMLHttpRequest();
        req.open("get", "https://fakestoreapi.com/products");
        req.send();
        req.addEventListener("readystatechange", () => {
            if(req.readyState === 4 && req.status >= 200 && req.status < 300){
               // console.log(JSON.parse(req.response));
                this.setState({
                    items: JSON.parse(req.response),
                });
            }
        });    
    }

    render() {
        const {items} = this.state;
        const destructionItems = items.map((el) =>{
            return {
                id: el.id,
                image: el.image,
                title: el.title,
                description: el.description,
                price: el.price,
                rating: el.rating,
                category: el.category,
            }
        })   
        return (
            <>
            <div className="container">
            {destructionItems.map((el,i) => {
                return <Item item={el} key={i+1}></Item>       
            })}
          </div>  
          </>
        )
    }
}

export default Card